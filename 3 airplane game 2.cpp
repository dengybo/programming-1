//实现发射子弹的功能，当二位数组canvass[High][width]中的元素值为2时输出子弹’|‘。改进玩家可以连续按键，在画面中会同时显示多发子弹.
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>
 
#define High 25            //游戏画面尺寸
#define Width 50

//全局变量
int position_x, position_y;   //飞机的位置
int canvas[High][Width] = { 0 };      //二维数组储存游戏画面中对应的元素，0为空格，1为飞机，2为子弹，3为敌机

void gotoxy(int x, int y)        //将光标移到（x，y）位置
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle, pos);
}

void startup()                //数据的初始化
{
	position_x = High / 2;
	position_y = Width / 2;
	canvas[position_x][position_y] = 1;
}

void show()               //显示画面
{
	gotoxy(0,0);          //光标移到原点位置，以下重画清屏
	int i, j;
	for (i = 0;  i< High; i++)
	{
		for (j = 0; j < Width; j++)
		{
			if (canvas[i][j] == 0)
				printf(" ");    //输出空格
			else if (canvas[i][j] == 1)
				printf("*");          //输出飞机
			else if (canvas[i][j] == 2)
				printf("|");         //输出子弹
		}
		printf("\n");
	}
}

void updateWithoutInput()      //与用户输入无关的更新
{
	int i, j;
	for (i = 0; i < High; i++)
	{
		for (j = 0; j < Width; j++)
		{
			if (canvas[i][j] ==2)              //子弹向上移动
			{
				canvas[i][j] = 0;
				if (i > 0)
					canvas[i - 1][j] = 2;
			}
		}
	}
}

void updateWithInput()        //与用户输入有关的更新
{
	char input;
	if (kbhit())
	{
		input = getch();
		if (input == 'a')
		{
			canvas[position_x][position_y] = 0;
			position_y--;            //位置左移
			canvas[position_x][position_y] = 1;
		}
		else if (input == 'd')
		{
			canvas[position_x][position_y] = 0;
			position_y++;            //位置右移
			canvas[position_x][position_y] = 1;
		}
		else if (input == 'w')
		{
			canvas[position_x][position_y] = 0;
			position_x--;            //位置上移
			canvas[position_x][position_y] = 1;
		}
		else if (input == 's')
		{
			canvas[position_x][position_y] = 0;
			position_x++;            //位置下移
			canvas[position_x][position_y] = 1;
		}
		else if (input == ' ')            //发射子弹
		{
			canvas[position_x - 1][position_y] = 2;             //发射子弹的初始位置在飞机的正上方
		}
	}
}

int main(void)
{
	startup();   //数据初始化
	while (1)
	{
		show();   //显示画面
		updateWithoutInput();                 //与用户输入无关的更新
		updateWithInput();                         //与用户输入有关的更新
	}
	return 0;
}
